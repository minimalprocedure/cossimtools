// Copyright (c) 2023 Massimo Ghisalberti
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

import {unique, zip, unzip} from './list';

export type TNumberVector = number[];
export type TStringVector = string[];
export type TNumberTupleVector = [number, number];
export type TNumbersTupleVector = [number[], number[]];
export type TBagOfFrequence<T> = [T, number];
export type TBagsOfFrequence<T> = TBagOfFrequence<T>[];
export type TBagOfPresences<T> = [T, TNumberTupleVector];
export type TBagsOfPresences<T> = TBagOfPresences<T>[];

export interface TCosineSimStruct {
  angle: number;
  magnDict: number;
  magnWords: number;
  dotProd: number;
  cosSim: number;
}

export function rawWords(text: String): TStringVector {
  const words = text.trim().split(/[\s\n\t:;,.'!?()<>¬’“””\\/&—\\-]+/gm);
  return words.map(w => w.toLowerCase()); //.sort();
}

export function filterWordsByLen(
  words: TStringVector,
  len: number
): TStringVector {
  return words.filter(w => w.length > len);
}

export function vocabulary(words: TStringVector): TStringVector {
  return unique(words);
}

export function bagsOfWordsByPresence<T>(
  dict: T[],
  words: T[]
): TBagsOfPresences<T> {
  const lemmas = unique(dict.concat(words)).sort();
  const v1: TNumberVector = new Array(lemmas.length).fill(0);
  const v2: TNumberVector = [...v1];
  const presences = zip(v1, v2);
  const bags: TBagsOfPresences<T> = zip(lemmas, presences);
  const build = (bag: TBagOfPresences<T>): TBagOfPresences<T> => (
    (bag[1][0] = dict.includes(bag[0]) ? 1 : 0),
    (bag[1][1] = words.includes(bag[0]) ? 1 : 0),
    bag
  );
  return bags.map(build);
}

export function bagsOfWordsByFreq<T>(words: T[]): [T, number][] {
  const last = (a: [T, number][]): [T, number] => a[a.length - 1];
  const count = (acc: [T, number][], w: T) => {
    const curr = last(acc);
    if (curr && curr[0] === w) {
      curr[1] += 1;
    } else {
      acc.push([w, 1]);
    }
    return acc;
  };
  return words.sort().reduce(count, []);
}

export function extractNumberVector<T>(
  bags: TBagsOfFrequence<T>
): TNumberVector {
  return bags.map(b => b[1]);
}

export function extractNumbersVectors<T>(
  bags: TBagsOfPresences<T>
): TNumbersTupleVector {
  const freq = bags.map(bag => bag[1]);
  return unzip(freq);
}

export function padVector(v: TNumberVector, size: number): TNumberVector {
  const padSize = size - v.length;
  return padSize < 0 ? v : v.concat(new Array(padSize).fill(0));
}

export function dotProduct(
  dWeights: TNumberVector,
  wWeights: TNumberVector
): number {
  const dicWeights = padVector(dWeights, wWeights.length);
  const mult = (ret: TNumberVector, tup: [number, number]): TNumberVector => (
    ret.push(tup[0] * tup[1]), ret
  );
  const sum = (a: number, b: number): number => a + b;
  return zip(dicWeights, wWeights).reduce(mult, []).reduce(sum, 0.0);
}

export function magnitude(v: TNumberVector): number {
  const pow = (acc: number, n: number): number => acc + Math.pow(n, 2.0);
  return Math.sqrt(v.reduce(pow, 0.0));
}

export function cosineSimilarity(
  dWeights: TNumberVector,
  wWeights: TNumberVector
): TCosineSimStruct {
  const magnDict = magnitude(dWeights);
  const magnWords = magnitude(wWeights);
  const dotProd = dotProduct(dWeights, wWeights);
  const cosSim = dotProd / (magnDict * magnWords);
  return {
    angle: Math.acos(cosSim) * (180 / Math.PI),
    magnDict: magnDict,
    magnWords: magnWords,
    dotProd: dotProd,
    cosSim: cosSim,
  };
}
