// Copyright (c) 2023 Massimo Ghisalberti
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

import {
  bagsOfWordsByPresence,
  filterWordsByLen,
  rawWords,
  extractNumbersVectors,
  cosineSimilarity,
} from './cosineSim';

const VOC1 = `
La frase oggi il sole splende, ma ma domani chi lo sa? Il sole splenderà?`;

const VOC2 = `
pippo pertica e palla, topolino, paperino, pluto e pippo, Immettere, combinata `;

const VOC3 = `
La frase oggi il sole splende, ma ma domani chi lo sa? Il sole splenderà? è
così ridotta a una lista di stringhe che contiene in questo caso un duplicato: sole. 
vocabolario. Questa lista andrà ulteriormente ristretta considerando che ci
servirà un vocabolario delle parole usate e qui un sole è di troppo.`;

const TEXT = `
La frase oggi il sole splende, ma ma domani chi lo sa? Il sole splenderà? è
così ridotta a una lista di stringhe che contiene in questo caso un duplicato: sole. 
vocabolario. Questa lista andrà ulteriormente ristretta considerando che ci
servirà un vocabolario delle parole usate e qui un sole è di troppo.`;

const LIMIT = 3;

export function main() {
  const raw_voc = rawWords(VOC2);
  const raw_words = rawWords(TEXT);

  const bagsPresences = bagsOfWordsByPresence(
    filterWordsByLen(raw_voc, LIMIT),
    filterWordsByLen(raw_words, LIMIT)
  );
  const [vecDict, vecLemma] = extractNumbersVectors(bagsPresences);

  const cosSim = cosineSimilarity(vecDict, vecLemma);

  console.log(cosSim);
}

main();
