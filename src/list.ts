// Copyright (c) 2023 Massimo Ghisalberti
// 
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

export function unique<T>(words: T[]): T[] {
  const unique = (v: T, i: number, a: T[]) => a.indexOf(v) === i && v !== '';
  return words.filter(unique);
}

export function zip<A, B>(a: A[], b: B[]): [A, B][] {
  return a.map((e: A, i: number) => [e, b[i]]);
}

export function unzip<A, B>(a: [A, B][]): [A[], B[]] {
  return a.reduce(
    (acc: [A[], B[]], v: [A, B]) => (acc[0].push(v[0]), acc[1].push(v[1]), acc),
    [[], []]
  );
}
